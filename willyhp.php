<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Willyhp</title>
</head>
<style>
    html{
        font-family: "Comic Sans MS", cursive, sans-serif;
    }
    .portofolio{
        text-align: center;
    }
    .grid{
        display: grid;
        grid-template-columns: 50% 50%;
        grid-column-gap: 1em;
        grid-row-gap: 1em;
    }
    .grid_2{
        display: grid;
        grid-template-columns: 20% 1% 79%;
        grid-column-gap: 1em;
        
    }
    .margin_p{
        margin-top: -20px;
    }
    .grid_3{
        display: grid;
        grid-template-columns: 35% 35% 30%;
        grid-column-gap: 1em;
        grid-row-gap: 1em;
        padding-left: 50px;
        padding-right: 50px;
        margin-top: -120px;
    }
    .grid_contact{
        display:grid;
        grid-template-columns: 30% 40% 30%;
    }
    .contact{
        text-align: center;
    }
    .email-form{
        padding: 2px;
        width: 100%;
        height: 25px;
        border-radius:5px;
    }
    .message-form{
        padding: 2px;
        width: 100%;
        height: 100px;
        border-radius:5px;
    }.button{
        border-radius: 5px;
        width: 75px;
        height: 35px;
        background-color: aquamarine;
    }
 
    
  

</style>
<body>
    <h1 class ="portofolio">MY PORTOFOLIO</h1>
    <hr>
    <div class="grid">
        <div>  <img src="51.jpeg" alt="" width="100%" height="600px"></div>
        <div> 
    <H2 >BIODATA</H2>
    <div class="grid_2">
        <div class ="margin_p"><b><p>NAMA</p></b></div>
        <div class ="margin_p"><p>:</p></div>
        <div class ="margin_p"><em><p>WILLY HANDOYO PUTRA</p></em></div>
        <div class ="margin_p"><b><p>HOBBY</p></b></div>
        <div class ="margin_p"><p>:</p></div>
        <div class ="margin_p"><em><p>GAME ONLINE</p></em></div>
        <div class ="margin_p"><b><p>TEMPAT LAHIR</p></b></div>
        <div class ="margin_p"><p>:</p></div>
        <div class ="margin_p"><em><p>PONTIANAK</p></em></div>
        <div class ="margin_p"><b><p>TANGGAL LAHIR</p></b></div>
        <div class ="margin_p"><p>:</p></div>
        <div class ="margin_p"><em><p>25 JANUARI 1998</p></em></div>
        <div class ="margin_p"><b><p>STATUS</p></b></div>
        <div class ="margin_p"><p>:</p></div>
        <div class ="margin_p"><em><p>FRESH GRADUATE</p></em></div>
        <div class ="margin_p"><b><p>PENDIDIKAN</p></b></div>
        <div class ="margin_p"><p>:</p></div>
        <div class ="margin_p"><em><p>SARJANA 1</p></em></div>
        <div class ="margin_p"><b><p>JURUSAN</p></b></div>
        <div class ="margin_p"><p>:</p></div>
        <div class ="margin_p"><em><p>TEKNIK INFORMATIKA</p></em></div>
    </div>
   
   </div>
    </div>
     <div class="grid_3">
         <div>
         <H4>Skills</H4>
    <ul>
        <li>php</li>
        <li>Javascript</li>
        <li>MySQL</li>
    </ul>
         </div>
         <div>
         <h4>Other Skills</h4>
    <ol>
        <li>Laravel</li>
    </ol>
         </div>
         <div>
         <h4>My Close Friend</h4>
    <ol>
        <li>Welly cipwanto</li>
        <li>Budi santoso</li>
        <li>Budi setiawan</li>
    </ol>
         </div>
     </div>
   
    
   
    <div class ="grid_contact">
        <div></div>
        <div>
        <h4 class ="contact">Contact Me</h4>
        <hr>
        <form action="#" method="post">
        <label for="" >Email :</label>
        <div style="height:5px;"></div>
        <input type="email" class ="email-form" placeholder="Write Email"><br>
        <label for="" >Message :</label>
        <div style="height:5px;"></div>
        <textarea name="" class ="message-form" placeholder="Write Something"></textarea><br>
        <input type="submit" value ="send" class ="button">
        </form>
        </div>
        <div></div>
   
 
    
</body>
</html>