class Table {
    constructor(init) {
        this.init = init;
    }
    createHeader(data) {
        let buka = "<thead><tr>";
        let tutup = "</thead></tr>";
        data.forEach((f) => {
            buka += `<th>${f}</th>`;
        });
        return buka + tutup;
    }
    createBody(data) {
        let buka = "<tbody>";
        let tutup = "</tbody>";
        data.forEach((d, index) => {
            buka += `<tr>`;
            d.forEach((datas) => {
                buka += `<td>${datas}</td>`
            });
            buka += '</tr>';
        })

        return buka + tutup;
    }
    render(element) {
        let table = "<table class ='table table-hover'>" +
            this.createHeader(this.init.columns) +
            this.createBody(this.init.data) +
            "</table>";
        element.innerHTML = table;
    }
}


const table = new Table({
    columns: ["nama", "email", "No Hanphone", "Alamat"],
    data: [
        ['Willyhp', 'limwilly0Gmail.com', '087734635840', 'kalimantan'],
        ['Budi', 'Budi@gmail.com', '08126677777', 'Kalimantan']
    ]
});

const app = document.getElementById("app");
table.render(app);