import { data, columns } from "./table.js";
class Table {
    constructor(init) {
        this.init = init;
    }
    createHeader(data) {
        let buka = "<thead>";
        let tutup = "</thead>";
        data.forEach((f) => {
            buka += `<tr>`;
            f.forEach(datas => {
                buka += `<th>${datas}</th>`;
            });
            buka += `</tr>`;
        });
        return buka + tutup;
    }
    createBody(data) {
        let buka = "<tbody>";
        let tutup = "</tbody>";
        data.forEach((d, index) => {
            buka += `<tr>`;
            d.forEach((datas) => {
                buka += `<td>${datas}</td>`
            });
            buka += '</tr>';
        })
        return buka + tutup;
    }
    render(element) {
        let table = "<table class ='table table-hover'>" +
            this.createHeader(this.init.columns) +
            this.createBody(this.init.data) +
            "</table>";
        element.innerHTML = table;
    }
}

function table() {
    const table = new Table({
        columns: columns(),
        data: data()
    });

    const app = document.getElementById("app");
    table.render(app);
}
table();